# This file contains all the required routines to make an A* search algorithm.
#
__authors__ = 'TO_BE_FILLED'
__group__ = 'DL01'
# _________________________________________________________________________________________
# Intel.ligencia Artificial
# Grau en Enginyeria Informatica
# Curs 2016- 2017
# Universitat Autonoma de Barcelona
# _______________________________________________________________________________________

from SubwayMap import *
import math


class Node:
    # __init__ Constructor of Node Class.
    def __init__(self, station, father):
        """
        __init__: 	Constructor of the Node class
        :param
                - station: STATION information of the Station of this Node
                - father: NODE (see Node definition) of his father
        """

        self.station = station  # STATION information of the Station of this Node
        self.father = father  # NODE pointer to his father

        if father == None:
            self.parentsID = []
        else:
            self.parentsID = [father.station.id]
            self.parentsID.extend(father.parentsID)  # TUPLE OF NODES (from the origin to its father)

        self.g = 0  # REsAL cost - depending on the type of preference -
        # to get from the origin to this Node
        self.h = 0  # REAL heuristic value to get from the origin to this Node
        self.f = 0  # REAL evaluate function
        self.time = 0  # REAL time required to get from the origin to this Node
        # [optional] Only useful for GUI
        self.num_stopStation = 0  # INTEGER number of stops stations made from the origin to this Node
        # [optional] Only useful for GUI
        self.distance = 0  # REAL distance made from the origin to this Node
        # [optional] Only useful for GUI
        self.transfers = 0  # INTEGER number of transfers made from the origin to this Node
        # [optional] Only useful for GUI

    def setEvaluation(self):
        """
        setEvaluation: 	Calculates the Evaluation Function. Actualizes .f value

        """
        self.f=self.g+self.h

    def setHeuristic(self, typePreference, station_destination, city):
        """"
        setHeuristic: 	Calculates the heuristic depending on the preference selected
        :params
                - typePreference: INTEGER Value to indicate the preference selected:
                                0 - Null Heuristic
                                1 - minimum Time
                                2 - minimum Distance
                                3 - minimum Transfers
                                4 - minimum Stops
                - node_destination: PATH of the destination station
                - city: CITYINFO with the information of the city (see CityInfo class definition)
        """
        
        if(typePreference == 1):
            self.h=math.sqrt(pow(station_destination.station.x-self.station.x,2)+pow(station_destination.station.y-self.station.y,2))/city.max_velocity
        elif(typePreference == 2):
            self.h=math.sqrt(pow(station_destination.station.x-self.station.x,2)+pow(self.station.y-station_destination.station.y,2))
      
        elif(typePreference == 3):
            if(self.station.line==station_destination.station.line):
                self.h=0
            else:
                
                if(station_destination.station.id in self.station.destinationDic):
                    self.h=1
                else:
                    lista_ids=[]
                    for element in self.station.destinationDic:
                         lista_ids.append(element)                  
                    lista_ids.append(self.station.id)
                    trobat=False
                    for idx,element in enumerate(lista_ids):
                        for e2 in city.StationList:
                            if(e2.x == city.StationList[element-1].x and e2.y == city.StationList[element-1].y and e2.id != city.StationList[element-1].id):
                                if(city.StationList[e2.id-1].line == station_destination.station.line):
                                    self.h=1
                                    trobat=True
                                    break
                                    
                                    
                   
                    if(not trobat):
                        self.h=2
        else:
            if(station_destination.station.x == self.station.x and station_destination.station.y==self.station.y):
                self.h=0
            elif(station_destination.station.id in self.station.destinationDic):
                    self.h=1
            else: self.h=2        
                               
            
       
                    
                    
               
                    
                    
                        
            
                        
                        
                
              

                
                        
                       
       
     

    def setRealCost(self, costTable):
        
        """
        setRealCost: 	Calculates the real cost depending on the preference selected
        :params
                 - costTable: DICTIONARY. Relates each station with their adjacency an their real cost. NOTE that this
                             cost can be in terms of any preference.
        """

        self.g = 0.0
        aux= self
        
        if self.father != None:
            self.g = self.father.g+costTable[aux.father.station.id][aux.station.id]
            
 

        
       
       
       


def coord2station(coord, stationList):
    import math
    import operator
    """
    coord2station :      From coordinates, it searches the closest station.
    :param
            - coord:  LIST of two REAL values, which refer to the coordinates of a point in the city.
            - stationList: LIST of the stations of a city. (- id, destinationDic, name, line, x, y -)

    :return:
            - possible_origins: List of the Indexes of the stationList structure, which corresponds to the closest
            station
    """
    distance={}
    
    for idx,station in enumerate(stationList):
        distance[idx]=math.sqrt(pow((station.x-coord[0]),2)+pow((station.y-coord[1]),2))
        
        
    #print "distance",(distance)
    sorted_distance=sorted(distance.items(), key=operator.itemgetter(1))
   

    distance_indexs=[sorted_distance[0][0]]

    for idx,element in enumerate(sorted_distance):
        
        if(element[1]==sorted_distance[0][1] and element[0] not in distance_indexs):
            distance_indexs.append(element[0])
      
            
    #print "distance_indexs",(distance_indexs)
    
    return(distance_indexs)

        
 


def Expand(fatherNode, city, station_destination=None, typePreference=0, costTable=None):
    """
        Expand: It expands a node and returns the list of connected stations (childrenList)
        :params
                - fatherNode: NODE of the current node that should be expanded
                - city: CITYINFO with the information of the city (see CityInfo class definition)
                - node_destination: NODE (see Node definition) of the destination
                - typePreference: INTEGER Value to indicate the preference selected:
                                0 - Null Heuristic
                                1 - minimum Time
                                2 - minimum Distance
                                3 - minimum Transfers
                                4 - minimum Stops
                - costTable: DICTIONARY. Relates each station with their adjacency an their real cost. NOTE that this
                             cost can be in terms of any preference.

        :returns
                - childrenList:  LIST of the set of child Nodes for this current node (fatherNode)

    """
  
    ChildrenNodeList=[]
   
    ids_child=list(city.StationList[(fatherNode.station.id-1)].destinationDic.keys())
    
    ids_child.sort()
    for i in (ids_child):
        son=Node(city.StationList[i-1],fatherNode)
        ChildrenNodeList.append(son)
        if(station_destination != None):
            son.setRealCost(costTable)
            son.setHeuristic(typePreference,station_destination, city);
            son.setEvaluation()
       
        
                                 
        #son.time+=(son.station.destinationDic[fatherNode.station.id]+fatherNode.time)               
        #son.walk+=((son.station.destinationDic[fatherNode.station.id])*(city.velocity_lines[son.station.line-1])+fatherNode.walk)                               
        if son.station.line != fatherNode.station.line:
            son.transfers=fatherNode.transfers+1
        else:
            son.transfers=fatherNode.transfers
        if son.station.id != fatherNode.station.id:
            son.num_stopStation=fatherNode.num_stopStation+1
        else:
            son.num_stopStation=fatherNode.num_stopStation
        
        

  
    
    
    return(ChildrenNodeList)    
        
  
        





def RemoveCycles(childrenList):
    """
        RemoveCycles: It removes from childrenList the set of childrens that include some cycles in their path.
        :params
                - childrenList: LIST of the set of child Nodes for a certain Node
        :returns
                - listWithoutCycles:  LIST of the set of child Nodes for a certain Node which not includes cycles
    """
    #removes the childnodes where de id is the same as the father, and if the coord are the same,then returns the list
    
    for idx,sonNode in enumerate(childrenList):
        
        if(sonNode.station.id in sonNode.parentsID):
            childrenList.pop(idx)
            
          
    return childrenList
    



def RemoveRedundantPaths(childrenList, nodeList, partialCostTable):
    """
        RemoveRedundantPaths:   It removes the Redundant Paths. They are not optimal solution!
                                If a node is visited and have a lower g in this moment, TCP is updated.
                                In case of having a higher value, we should remove this child.
                                If a node is not yet visited, we should include to the TCP.
        :params
                - childrenList: LIST of NODES, set of childs that should be studied if they contain rendundant path
                                or not.
                - nodeList : LIST of NODES to be visited
                - partialCostTable: DICTIONARY of the minimum g to get each key (Node) from the origin Node
        :returns
                - childrenList: LIST of NODES, set of childs without rendundant path.
                - nodeList: LIST of NODES to be visited updated (without redundant paths)
                - partialCostTable: DICTIONARY of the minimum g to get each key (Node) from the origin Node (updated)
    """
   
    for child in childrenList:
        if(child.station.id in partialCostTable):
            if(partialCostTable[child.station.id] >= child.g):
                partialCostTable[child.station.id]=child.g
                for element in childrenList:
                    for el in nodeList:
                        if(element.station.id == el.station.id):
                            nodeList.remove(el)
                            break
                #buscar el el children correspondiente a child en nodelist.
            else:
                childrenList.remove(child)
        else:
               
            partialCostTable[child.station.id]=child.g
                
                
    return childrenList,nodeList,partialCostTable            
                
           
                
                

def orderbyF(node):
    return node.f
    
def sorted_insertion(nodeList, childrenList):
    """ Sorted_insertion: 	It inserts each of the elements of childrenList into the nodeList.
                            The insertion must be sorted depending on the evaluation function value.

        : params:
            - nodeList : LIST of NODES to be visited
            - childrenList: LIST of NODES, set of childs that should be studied if they contain rendundant path
                                or not.
        :returns
                - nodeList: sorted LIST of NODES to be visited updated with the childrenList included
    """
    for node in childrenList:
        nodeList.append(node)
        
    nodeList.sort(key=orderbyF)

    return nodeList    

        
def setCostTable(typePreference, city):
    """
    setCostTable :      Real cost of a travel.
    :param
            - typePreference: INTEGER Value to indicate the preference selected:
                                0 - Adjacency
                                1 - minimum Time
                                2 - minimum Distance
                                3 - minimum Transfers
                                4 - minimum Stops
            - stationList: LIST of the stations of a city. (- id, destinationDic, name, line, x, y -)
            - city: CITYINFO with the information of the city (see CityInfo class definition)
    :return:
            - costTable: DICTIONARY. Relates each station with their adjacency an their g, depending on the
                                 type of Preference Selected.
    """
    costTable={}
    if(typePreference == 1):
        for idx,station in enumerate(city.StationList):
            costTable[idx+1]=city.StationList[idx].destinationDic
    elif(typePreference == 2):
        for idx,station in enumerate(city.StationList):
            costTable[idx+1]={}
            for element in city.StationList[idx].destinationDic:
                if(not(city.StationList[element-1].x==station.x and city.StationList[element-1].y==station.y)):
                    distance=city.StationList[idx].destinationDic[element]*city.velocity_lines[city.StationList[idx].line-1]
                    costTable[idx+1][element]=distance
                else:
                    costTable[idx+1][element]=0  
    elif(typePreference == 3):
         for idx,station in enumerate(city.StationList):
            costTable[idx+1]={}
            for element in city.StationList[idx].destinationDic:
                if(station.line == city.StationList[element-1].line):
                    costTable[idx+1][element]=0
                else:
                    costTable[idx+1][element]=1
    else:
        for idx,station in enumerate(city.StationList):
            costTable[idx+1]={}
            for element in city.StationList[idx].destinationDic:
                if(station.x == city.StationList[element-1].x and station.y == city.StationList[element-1].y ):
                    costTable[idx+1][element]=0
                else:
                    costTable[idx+1][element]=1
         
                      
    
    return costTable
       
            
        
  
      
            


def AstarAlgorithm(coord_origin, coord_destination, typePreference, city, flag_redundants):
    """
     AstarAlgorithm: main function. It is the connection between the GUI and the AStar search code.
     INPUTS:
            - stationList: LIST of the stations of a city. (- id, name, destinationDic, line, x, y -)
            - coord_origin: TUPLE of two values referring to the origin coordinates
            - coord_destination: TUPLE of two values referring to the destination coordinates
            - typePreference: INTEGER Value to indicate the preference selected:
                                0 - Adjacency
                                1 - minimum Time
                                2 - minimum Distance
                                3 - minimum Transfers
                                4 - minimum Stops
            - city: CITYINFO with the information of the city (see CityInfo class definition)
			- flag_redundants: [0/1]. Flag to indicate if the algorithm has to remove the redundant paths (1) or not (0)

    OUTPUTS:
            - time: REAL total required time to make the route
            - distance: REAL total distance made in the route
            - transfers: INTEGER total transfers made in the route
            - stopStations: INTEGER total stops made in the route
            - num_expanded_nodes: INTEGER total expanded nodes to get the optimal path
            - depth: INTEGER depth of the solution
            - visitedNodes: LIST of INTEGERS, IDs of the stations corresponding to the visited nodes
            - idsOptimalPath: LIST of INTEGERS, IDs of the stations corresponding to the optimal path
            (from origin to destination)
            - min_distance_origin: REAL the distance of the origin_coordinates to the closest station
            - min_distance_destination: REAL the distance of the destination_coordinates to the closest station



            EXAMPLE:
            return optimalPath.time, optimalPath.walk, optimalPath.transfers,optimalPath.num_stopStation,
            len(expandedList), len(idsOptimalPath), visitedNodes, idsOptimalPath, min_distance_origin,
            min_distance_destination
    """
    og=[]
    dest=[]
    og=coord2station(coord_origin, city.StationList)
    dest=coord2station(coord_destination, city.StationList)
    
    NodeOrigin=Node(city.StationList[og[0]],None)
    NodeDes=Node(city.StationList[dest[0]],None)
    costTable=setCostTable(int(typePreference),city)
    
    Lista=[NodeOrigin]
#def Expand(fatherNode, city, station_destination=None, typePreference=0, costTable=None):    
    while Lista != None and Lista[0].station.id != NodeDes.station.id:
        C=Lista[0]
        E=Expand(C,city,NodeDes,int(typePreference),costTable)
        E=RemoveCycles(E)
        print("here")
        #if(flag_redundants == 1):
        #E=RemoveRedundantPaths(childrenList,nodeList,costTable)
        Lista=sorted_insertion(Lista[1:],E)
    
    
    lista_ids=[]
    #print(Lista[0][0].father.station.id)
    nodeAux=Lista[0].father
    lista_ids.append(Lista[0].station.id)

    while nodeAux != None:                      
        lista_ids.append(nodeAux.station.id)
        nodeAux=nodeAux.father
        print("here")
    lista_ids.reverse()   
    if Lista == None:
        return (0,0,0,0,0,0,lista_ids,lista_ids,0,0)
    else:
        return (0,0,0,0,0,0,lista_ids,lista_ids,0,0)
        print("Cap,sol")
        
        
   
    
    



